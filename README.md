Atreo/Whatsapp
===========================


Installation
------------

```sh
$ composer require atreo/whatsapp
```

Extension configuration:

```
whatsapp: Atreo\Whatsapp\DI\WhatsappExtension

whatsapp:
	phoneNumber: 420*********
	nickName: myNickName
	password: ******
    debug: true
    dataDir: '%tempDir%/whatsapp',
    log: true

```

Usage:

```

/**
 * @inject
 * @var \Atreo\Whatsapp\Whatsapp
 */
public $whatsapp;

// ...

$this->whatsapp->sendMessage('420111331133', 'Test message');

```

