<?php

namespace Atreo\Whatsapp\DI;

use Nette\DI\CompilerExtension;
use Nette\Utils\Validators;



/**
 * @author Martin Pánek <kontakt@martinpanek.cz>
 */
class WhatsappExtension extends CompilerExtension
{

	/**
	 * @var array
	 */
	private $defaults = [
		'debug' => FALSE,
		'dataDir' => '%tempDir%/whatsapp',
		'log' => TRUE
	];



	public function loadConfiguration()
	{
		$config = $this->getConfig($this->defaults);

		Validators::assertField($config, 'phoneNumber', 'integer|string');
		Validators::assertField($config, 'nickName', 'string');
		Validators::assertField($config, 'password', 'string');
		Validators::assertField($config, 'debug', 'boolean');

		Validators::assertField($config, 'dataDir', 'string');
		Validators::assertField($config, 'log', 'boolean');

		$builder = $this->getContainerBuilder();

		$builder->addDefinition($this->prefix('whatsapp'))
			->setClass('Atreo\Whatsapp\Whatsapp', [$config]);

	}

}
