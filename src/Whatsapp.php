<?php

namespace Atreo\Whatsapp;

use Nette\Object;



/**
 * @author Martin Pánek <kontakt@martinpanek.cz>
 */
class Whatsapp extends Object
{

	/**
	 * @var \WhatsProt
	 */
	private $client;

	/**
	 * @var bool
	 */
	private $isConnected = FALSE;

	/**
	 * @var string
	 */
	private $password;



	/**
	 * @param array $config
	 */
	public function __construct(array $config)
	{
		$this->client = new \WhatsProt(
			$config['phoneNumber'],
			$config['nickName'],
			$config['debug'],
			$config['log'],
			$config['dataDir']
		);
		$this->password = $config['password'];
	}



	/**
	 * @return \WhatsProt
	 */
	public function getClient()
	{
		return $this->client;
	}



	private function connect()
	{
		if ($this->isConnected) {
			return;
		}

		$this->client->connect();
		$this->client->loginWithPassword($this->password);
		$this->isConnected = TRUE;
	}



	/**
	 * @param string $phoneNumber
	 * @param string $message
	 * @return string
	 */
	public function sendMessage($phoneNumber, $message)
	{
		$this->connect();
		return $this->client->sendMessage($phoneNumber , $message);
	}



	/**
	 * @param string $phoneNumber
	 * @param string $pathOrUrl
	 * @param string $caption
	 * @param int $fileSize
	 * @param string $fileHash
	 * @throws \ConnectionException
	 */
	public function sendVideoMessage($phoneNumber, $pathOrUrl, $caption = '', $fileSize = 0, $fileHash = '')
	{
		$this->client->sendMessageVideo($phoneNumber, $pathOrUrl, FALSE, $fileSize, $fileHash, $caption);

		try {
			$this->client->pollMessage();
		} catch (\ConnectionException $e) {
			$this->connect();
			$this->client->pollMessage();
		}
	}



	/**
	 * @param string $phoneNumber
	 * @param string $pathOrUrl
	 * @param string $caption
	 * @param int $fileSize
	 * @param string $fileHash
	 * @throws \ConnectionException
	 */
	public function sendImageMessage($phoneNumber, $pathOrUrl, $caption = '', $fileSize = 0, $fileHash = '')
	{
		$this->client->sendMessageImage($phoneNumber, $pathOrUrl, FALSE, $fileSize, $fileHash, $caption);

		try {
			$this->client->pollMessage();
		} catch (\ConnectionException $e) {
			$this->connect();
			$this->client->pollMessage();
		}
	}



	/**
	 * @param string $phoneNumber
	 * @param string $pathOrUrl
	 * @param string $caption
	 * @param int $fileSize
	 * @param string $fileHash
	 * @param bool $voice
	 * @throws \ConnectionException
	 */
	public function sendAudioMessage($phoneNumber, $pathOrUrl, $caption = '', $fileSize = 0, $fileHash = '', $voice = FALSE)
	{
		$this->client->sendMessageAudio($phoneNumber, $pathOrUrl, FALSE, $fileSize, $fileHash, $voice);

		try {
			$this->client->pollMessage();
		} catch (\ConnectionException $e) {
			$this->connect();
			$this->client->pollMessage();
		}
	}

}
